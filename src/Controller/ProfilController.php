<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\ProfilType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function profil(Security $security, Request $request, UserPasswordEncoderInterface $encoder, ValidatorInterface $validator): Response
    {
    // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
    $id = $security->getUser()->getId();
    // On récupère la repository des User et on va chercher l'utilisateur par son id
    $repository = $this->getDoctrine()->getRepository(User::class);
    $user = $repository->find($id);
    // On récupère le nom de l'image d'avatar de l'utilisateur
    if(!is_null($user->getAvatar())){
        $imageAvatar = $user->getAvatar()->getImage();
    }else{
        $imageAvatar = null;
    }
    // Mise en place du formulaire
    $form = $this->createForm(ProfilType::class, $user);
    // On vérifie si on a des données postées dans la requête, ce qui signifierait que l'on est en mode vérification de formulaire et pas affichage simple
    $form->handleRequest($request);
    
    if ($form->isSubmitted() && $form->isValid()) {
        // dd($user);
        // On récupère ensuite le manager d'entity qui permet les transactions avec la BDD
        $entityManager = $this->getDoctrine()->getManager();
        // Prise en charge d'un changement potentiel de password
        $newPassword = $request->request->get('newPassword');
        // On vérifie si la donnée n'est pas vide
        if(!empty($newPassword)){
            // On encode le nouveau password
            $password = $encoder->encodePassword($user, $newPassword);
            // On met à jour la propriété du user
            $user->setPassword($password);
        }
        // Gestion de l'avatar
        if(is_null($user->getAvatar()->getImageFile())){
            $user->getAvatar()->setImage($request->request->get('oldAvatar'));
        }
        // Gestion des asserts (contraintes) de l'avatar
        $errors = $validator->validate($user->getAvatar());
        if(count($errors) > 0){
            return $this->render('profil/profil.html.twig', [
                'form' => $form->createView(),
                'imgAvatar' => $imageAvatar,
                'errors' => $errors
            ]);
        }
        // On mémorise le contact pour une mise en base de données future
        $entityManager->persist($user);
        // On envoie en bdd
        $entityManager->flush();
        // On met en place un flashMessage de Symfony 
        $this->addFlash('success', 'Vos modifications ont bien été enregistrées.');
        return $this->redirectToRoute('profil');
    }
    // Rendu
    return $this->render('profil/profil.html.twig', [
        'form' => $form->createView(),
        'imgAvatar' => $imageAvatar
    ]);
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        // Mise en place des éléments pour renvoyer un formulaire à la vue
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // On ajoute le rôle user à l'utilisateur
            $user->setRoles(["ROLE_USER"]);
            // On prend en charge l'encodage du password
            // $passwordOrigine = $user->getPassword();
            // $passwordEncode = $encoder->encodePassword($user, $passwordOrigine);
            $user->setPassword('password');
            // On récupère ensuite le manager d'entity qui permet les transactions avec la BDD
            $entityManager = $this->getDoctrine()->getManager();
            // On mémorise le contact pour une mise en base de données future
            $entityManager->persist($user);
            // On envoie en bdd
            $entityManager->flush();
            // return new Response("Utilisateur correctement ajouté");
            return $this->redirectToRoute('profil');
        }
        // X. On retourne le rendu de la vue en lui passant en paramètre la création de la vue html correspondant au modèle du formulaire($form)
        return $this->render('profil/inscription.html.twig', [
            "form" => $form->createView()
        ]);
    }
}
