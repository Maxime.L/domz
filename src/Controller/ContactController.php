<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, Security $security): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Mise en base de données
            $entityManager = $this->getDoctrine()->getManager();
            // On mémorise le contact pour une mise en base de données future
            $entityManager->persist($contact);
            // On envoie en bdd
            $entityManager->flush();
            // On redirige vers la page de confirmation
            return $this->redirectToRoute('contact_confirmation');
        }
        if(!is_null($security->getUser())){
        // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
        $id = $security->getUser()->getId();
        // On récupère la repository des User et on va chercher l'utilisateur par son id
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($id);
        // On récupère le nom de l'image d'avatar de l'utilisateur
        if (!is_null($user->getAvatar())) {
            $imageAvatar = $user->getAvatar()->getImage();
        } else {
            $imageAvatar = null;
        }
        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView(),
            'imgAvatar' => $imageAvatar
        ]);
    }else{
        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
    }
    /**
     * @Route("/contact/confirmation", name="contact_confirmation")
     */
    public function confirmation()
    {
        return $this->render("contact/confirmation.html.twig");
    }
}
