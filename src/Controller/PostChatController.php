<?php

namespace App\Controller;

use App\Form\PostType;
use App\Entity\PostChat;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PostChatController extends AbstractController
{
    /**
     * @Route("/post/chat", name="post_chat")
     */
    public function index(Security $security): Response
    {
        $repository = $this->getDoctrine()->getRepository(PostChat::class);
        $posts = $repository->findAll();
        if (!is_null($security->getUser())) {
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
            return $this->render('post_chat/index.html.twig', [
                'posts' => $posts,
                'imgAvatar' => $imageAvatar
            ]);
        } else {
            return $this->render('post_chat/index.html.twig', [
                'posts' => $posts,
            ]);
        }
    }
    /**
     * @Route("/post/ajout", name="post_chat_ajout")
     */
    public function addPostChat(Security $security, Request $request): Response
    {
        $user = $security->getUser();
        $addPost = new PostChat();
        // 2. On instancie un formulaire de contact
        $form = $this->createForm(PostType::class, $addPost);
        // 3. On hydrate le formulaire avec les potentielles données se trouvant dans la requête
        $form->handleRequest($request);
        // 4. On vérifie s'il y a des données conformes dans le formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            // Mise en base de données
            $addPost->setUser($user);
            // On demande au contrôleur ($this) de récupérer l'instance de Doctrine à l'aide de la méthode getDoctrine() héritée de AbstractController(le contrôleur étant
            // AbstractController)
            // On récupère ensuite le manager d'entity qui permet les transactions avec la BDD
            $entityManager = $this->getDoctrine()->getManager();
            // On mémorise le contact pour une mise en base de données future
            $entityManager->persist($addPost);
            // On envoie en bdd
            $entityManager->flush();
            // On redirige vers la page de confirmation
            return $this->redirectToRoute('post_chat');
        }
        if (!is_null($security->getUser())) {
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
            return $this->render('post_chat/post_chat_ajout.html.twig', [
                'form' => $form->createView(),
                'imgAvatar' => $imageAvatar
            ]);
        } else {
            return $this->render('post_chat/post_chat_ajout.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }
}
