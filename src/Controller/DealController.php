<?php

namespace App\Controller;

use index;
use App\Entity\Deal;
use App\Entity\User;
use App\Form\DealType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DealController extends AbstractController
{
    /**
     * Requête à la base de données pour obtenir la liste des deals
     * @Route("/deal", name="deal")
     */
    public function index(Request $request, PaginatorInterface $paginator, Security $security): Response
    {
        // Récupération des Deals dans la BDD
        $donnees = $this->getDoctrine()->getRepository(Deal::class)->findBy(array(), ['createdAt' => 'DESC']);
        $deals = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            4 // Nombre de résultats par page
        );
        $repository = $this->getDoctrine()->getRepository(Deal::class);
        $lastDeals = $repository->findBy(array(), ['createdAt' => 'DESC'], 3);
        
        // Récupération du user et de son avatar
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('deal/deal.html.twig', [
            'deals' => $deals,
            'lastDeals' => $lastDeals,
            'imgAvatar' => $imageAvatar
        ]);
    }else{
        return $this->render('deal/deal.html.twig', [
            'deals' => $deals,
            'lastDeals' => $lastDeals
        ]);
    }
    }
    /**
     * Requête à la base de données pour obtenir les trois derniers deals
     * @Route("/majDeal", name="maj-deal")
     */
    public function majDeal(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Deal::class);
        $lastDeals = $repository->findBy(array(), ['createdAt' => 'DESC'], 3);
        // return new Response('coucou');
        return $this->render('deal/colonne-deal.html.twig', [
            'lastDeals' => $lastDeals
        ]);
    }

    /**
     * Requête à la base de données pour obtenir le détail d'un deal
     * @Route("/deal/{slug}", name="deal-detail")
     * @return Response
     */
    public function dealDetail($slug, Security $security): Response
    {
        $repository = $this->getDoctrine()->getRepository(Deal::class);
        $deal = $repository->find($slug);
        $lastDeals = $repository->findBy(array(), ['createdAt' => 'DESC'], 3);

        // Récupération du user et de son avatar
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('deal/deal-detail.html.twig', ["deal" => $deal, 'lastDeals' => $lastDeals, 'imgAvatar' => $imageAvatar]);
    }else{
        return $this->render('deal/deal-detail.html.twig', ["deal" => $deal, 'lastDeals' => $lastDeals]);
    }
}

    /** Requête à la base de données pour obtenir le formulaire d'ajout
     * @Route("/ajout", name="ajout-deal")
     */
    public function addDeal(Security $security, Request $request): Response
    {
        // Récupération du user et de son avatar
        // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
        $id = $user = $security->getUser();
        // On récupère la repository des User et on va chercher l'utilisateur par son id
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->find($id);
        // On récupère le nom de l'image d'avatar de l'utilisateur
        if (!is_null($user->getAvatar())) {
            $imageAvatar = $user->getAvatar()->getImage();
        } else {
            $imageAvatar = null;
        }

        // On matérialise un formulaire de contact pour le passer à la vue
        // 1. On instancie un objet de la classe(entity) Contact
        $addDeal = new Deal();
        // 2. On instancie un formulaire de contact
        $form = $this->createForm(DealType::class, $addDeal);
        // 3. On hydrate le formulaire avec les potentielles données se trouvant dans la requête
        $form->handleRequest($request);
        // 4. On vérifie s'il y a des données conformes dans le formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $addDeal->setUser($user);
            // Mise en base de données
            // On demande au contrôleur ($this) de récupérer l'instance de Doctrine à l'aide de la méthode getDoctrine() héritée de AbstractController(le contrôleur étant
            // AbstractController)
            // On récupère ensuite le manager d'entity qui permet les transactions avec la BDD
            $entityManager = $this->getDoctrine()->getManager();
            // On mémorise le contact pour une mise en base de données future
            $entityManager->persist($addDeal);
            // On envoie en bdd
            $entityManager->flush();
            // On redirige vers la page de confirmation
            return $this->redirectToRoute('deal');
        }
        // X. On retourne le rendu de la vue en lui passant en paramètre la création de la vue html correspondant au modèle du formulaire($form)
        return $this->render('deal/ajout-deal.html.twig', [
            'form' => $form->createView(),
            'imgAvatar' => $imageAvatar
        ]);
    }
}
