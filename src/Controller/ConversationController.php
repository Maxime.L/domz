<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Entity\PostChat;
use App\Form\ConversationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConversationController extends AbstractController
{
    /**
     * @Route("/chat", name="chat")
     */
    public function index(Security $security): Response
    {
        $repository = $this->getDoctrine()->getRepository(Conversation::class);
        $conversations = $repository->findAll();
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('post_chat/chat.html.twig', [
            'conversations' => $conversations,
            'imgAvatar' => $imageAvatar
        ]);
        }else{
            return $this->render('post_chat/chat.html.twig', [
                'conversations' => $conversations,
            ]);
        }
    }
    /**
     * @Route("/chat/{id}", name="post_chat_form")
     */
    public function addConversation(Security $security, Request $request, $id): Response
    {
        //
        $repository = $this->getDoctrine()->getRepository(PostChat::class);
        $post = $repository->find($id);
        //
        $repository = $this->getDoctrine()->getRepository(Conversation::class);
        $conversations = $repository->findBy(array('posts'=>$post->getId()), array('createdAt'=>"DESC"));
        //
        $user = $security->getUser();
        $addConversation = new Conversation();
        // 2. On instancie un formulaire de contact
        $form = $this->createForm(ConversationType::class, $addConversation); 
        // 3. On hydrate le formulaire avec les potentielles données se trouvant dans la requête
        $form->handleRequest($request);
        // 4. On vérifie s'il y a des données conformes dans le formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            // Mise en base de données
            $addConversation->setUser($user);
            $addConversation->setPosts($post);
            // On demande au contrôleur ($this) de récupérer l'instance de Doctrine à l'aide de la méthode getDoctrine() héritée de AbstractController(le contrôleur étant
            // AbstractController)
            // On récupère ensuite le manager d'entity qui permet les transactions avec la BDD
            $entityManager = $this->getDoctrine()->getManager();
            // On mémorise le contact pour une mise en base de données future
            $entityManager->persist($addConversation);
            // On envoie en bdd
            $entityManager->flush();
            // On redirige vers la page de confirmation
            return $this->redirectToRoute('post_chat_form', array("id"=>$id));
        }
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('post_chat/chat.html.twig', [
            'form' => $form->createView(),
            'conversations' => $conversations,
            'imgAvatar' => $imageAvatar
        ]);
    }else{
        return $this->render('post_chat/chat.html.twig', [
            'form' => $form->createView(),
            'conversations' => $conversations,
        ]);
    }
}
}