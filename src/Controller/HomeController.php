<?php

namespace App\Controller;

use App\Entity\Home;
use App\Entity\News;
use App\Entity\User;
use App\Entity\Carousel;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     * @Route("/", name="index")
     */
    public function index(Security $security): Response
    {
        $repository = $this->getDoctrine()->getRepository(Home::class);
        $homeA = $repository->findOneBy(array("active"=>1, "localisation" => "A"));
        $homeB = $repository->findOneBy(array("active"=>1, "localisation" => "B" ));
        $homeC = $repository->findOneBy(array("active"=>1, "localisation" => "C" ));
        $repository = $this->getDoctrine()->getRepository(Carousel::class);
        $carousel = $repository->findBy(array("active"=>1));

        // Récupération du user et de son avatar
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('home/home.html.twig', [
            'homeA' => $homeA,
            'homeB' => $homeB,
            'homeC' => $homeC,
            'slides' => $carousel,
            'imgAvatar' => $imageAvatar
        ]);
    }else{
        return $this->render('home/home.html.twig', [
            'homeA' => $homeA,
            'homeB' => $homeB,
            'homeC' => $homeC,
            'slides' => $carousel
        ]);
    }
}
}
