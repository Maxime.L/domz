<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\User;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;

class NewsController extends AbstractController
{
    /**
     * Requête à la base de données pour obtenir la liste des news
     * @Route("/news", name="news")
     */
    public function index(Request $request, PaginatorInterface $paginator, Security $security): Response
    {
        // On place les news les plus récent en première page
        $donnees = $this->getDoctrine()->getRepository(News::class)->findBy(array(),["createdAt"=>"desc"]);
        $news = $paginator->paginate(
            $donnees,
            $request->query->getInt('page', 1),
            3
        );
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('news/news.html.twig', [
            'news' => $news,
            'imgAvatar' => $imageAvatar
        ]);
    }else{
        return $this->render('news/news.html.twig', [
            'news' => $news
        ]);
    }
}

        /**
     * Requête à la base de données pour obtenir le détail d'un news
     * @Route("/news/{slug}", name="news-detail")
     * @return Response
     */
    public function newsDetail($slug, Security $security):Response{
        $repository = $this->getDoctrine()->getRepository(News::class);
        $news = $repository->find($slug);    
        $lastnews = $repository->findBy(array(),["createdAt"=>"desc"], 4);
        if(!is_null($security->getUser())){
            // Avec le service Security, on récupère le user connecté pour obtenir ses infos à modifier
            $id = $security->getUser()->getId();
            // On récupère la repository des User et on va chercher l'utilisateur par son id
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($id);
            // On récupère le nom de l'image d'avatar de l'utilisateur
            if (!is_null($user->getAvatar())) {
                $imageAvatar = $user->getAvatar()->getImage();
            } else {
                $imageAvatar = null;
            }
        return $this->render('news/news-detail.html.twig', ["news"=>$news, "lastnews" => $lastnews, 'currentId'=>$slug, 'imgAvatar' => $imageAvatar] );
    }else{
        return $this->render('news/news-detail.html.twig', ["news"=>$news, "lastnews" => $lastnews, 'currentId'=>$slug] );
    }
}
}