<?php

namespace App\Repository;

use App\Entity\CategorieDeal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategorieDeal|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieDeal|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieDeal[]    findAll()
 * @method CategorieDeal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieDealRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieDeal::class);
    }

    // /**
    //  * @return CategorieDeal[] Returns an array of CategorieDeal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategorieDeal
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
