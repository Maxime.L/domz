<?php

namespace App\Repository;

use App\Entity\PostChat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostChat|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostChat|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostChat[]    findAll()
 * @method PostChat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostChat::class);
    }

    // /**
    //  * @return PostChat[] Returns an array of PostChat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostChat
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
