<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture {
    // Propriétés
    // On déclare une propriété privée pour le passwordEncoder
    private $encoder;
    public const USER_REFERENCE = 'user';
    public const USER_ADMIN_REFERENCE = 'admin';
    // Constructeur
    // On fait une injection de dépendance dans le constructeur de la classe, c'est à dire qu'on passe un paramètre dont Symfony se charge à l'instanciation
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('user@user.com');
        // $passwordOrigine = "password";
        // $passwordEncode = $this->encoder->encodePassword($user, $passwordOrigine);
        $user->setpassword("password");
        // LES INFORMATIONS DU USER
        // LE ROLE
        $user->setRoles(['ROLE_USER']);
        // NOM ET PRENOM
        $user->setNom("User");
        $user->setPrenom("Pierre");
        // Adresse
        $user->setAdresse("1 Adresse du Paradis");
        $user->setCp("78200");
        $user->setVille("Mantes-La-Jolie");
        // Objet en BDD
        $manager->persist($user);
        $this->addReference(self::USER_REFERENCE, $user);

        $user = new User();
        $user->setEmail('userbis@user.com');
        // $passwordOrigine = "password";
        // $passwordEncode = $this->encoder->encodePassword($user, $passwordOrigine);
        $user->setpassword("password");
        // LES INFORMATIONS
        // LE ROLE
        $user->setRoles(['ROLE_USER']);
        // NOM ET PRENOM
        $user->setNom("Lamy");
        $user->setPrenom("Maxime");
        // Adresse
        $user->setAdresse("2 Adresse du Paradis");
        $user->setCp("78200");
        $user->setVille("Mantes-La-Jolie");
        // Objet en BDD
        $manager->persist($user);

        $user = new User();
        $user->setEmail('userter@user.com');
        // $passwordOrigine = "password";
        // $passwordEncode = $this->encoder->encodePassword($user, $passwordOrigine);
        $user->setpassword("password");
        // LES INFORMATIONS
        // LE ROLE
        $user->setRoles(['ROLE_USER']);
        // NOM ET PRENOM
        $user->setNom("Meneux");
        $user->setPrenom("Christian");
        // Adresse
        $user->setAdresse("3 Adresse du Paradis");
        $user->setCp("78200");
        $user->setVille("Mantes-La-Jolie");
        // Objet en BDD
        $manager->persist($user);

        $user = new User();
        $user->setEmail('admin@admin.com');
        // $passwordOrigine = "password";
        // $passwordEncode = $this->encoder->encodePassword($user, $passwordOrigine);
        $user->setpassword("password");
        // LES INFORMATIONS
        // LE ROLE
        $user->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        // NOM ET PRENOM
        $user->setNom("Jesuis");
        $user->setPrenom("Admin");
        // Adresse
        $user->setAdresse("4 Adresse du Paradis");
        $user->setCp("78200");
        $user->setVille("Mantes-La-Jolie");
        $this->addReference(self::USER_ADMIN_REFERENCE, $user);
        // Objet en BDD
        $manager->persist($user);
        // ON ENVOI EN BDD
        $manager->flush();
        //On fait un php bin/console make:user pour sécuriser nos rôles
    }
}
