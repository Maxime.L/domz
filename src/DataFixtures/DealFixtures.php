<?php

namespace App\DataFixtures;

use App\Entity\Deal;
use App\Entity\CategorieDeal;
use App\DataFixtures\UserFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class DealFixtures extends Fixture implements DependentFixtureInterface 
{
    public function load(ObjectManager $manager)
    {
        $categorie = new CategorieDeal();
        $categorie->setNom('alimentation');
        $manager->persist($categorie);

        for ($i = 0; $i < 4; $i++) {
            $deal = new Deal();
            $deal->setTitre('deal ' . $i);
            $deal->setDescription("L'alimentation..... lorem ipsum");
            $deal->setCategorie($categorie);
            $deal->setImage('image' . $i);
            $deal->setPrix(mt_rand(1, 25));
            $deal->setCreatedAt(new \DateTime('now'));
            $deal->setUpdatedAt(new \DateTime('now'));
            $deal->setExpire(new \DateInterval('P2Y4DT6H8M'));
            $deal->setUser($this->getReference(UserFixtures::USER_REFERENCE));
            $manager->persist($deal);
        }

        $categorie = new CategorieDeal();
        $categorie->setNom('électronique');
        $manager->persist($categorie);

        for ($i = 0; $i < 4; $i++) {
            $deal = new Deal();
            $deal->setTitre('deal ' . $i);
            $deal->setDescription("L'électronique..... lorem ipsum");
            $deal->setCategorie($categorie);
            $deal->setImage('image' . $i);
            $deal->setPrix(mt_rand(1, 25));
            $deal->setCreatedAt(new \DateTime('now'));
            $deal->setUpdatedAt(new \DateTime('now'));
            $deal->setExpire(new \DateInterval('P2Y4DT6H8M'));
            $deal->setUser($this->getReference(UserFixtures::USER_REFERENCE));
            $manager->persist($deal);
        }

        $categorie = new CategorieDeal();
        $categorie->setNom('divers');
        $manager->persist($categorie);
        for ($i = 0; $i < 4; $i++) {
            $deal = new Deal();
            $deal->setTitre('deal ' . $i);
            $deal->setDescription("Les divers..... lorem ipsum");
            $deal->setCategorie($categorie);
            $deal->setImage('image' . $i);
            $deal->setPrix(mt_rand(1, 25));
            $deal->setCreatedAt(new \DateTime('now'));
            $deal->setUpdatedAt(new \DateTime('now'));
            $deal->setExpire(new \DateInterval('P2Y4DT6H8M'));
            $deal->setUser($this->getReference(UserFixtures::USER_REFERENCE));
            $manager->persist($deal);
        }

        // 
        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
