<?php

namespace App\DataFixtures;

use App\Entity\News;
use App\DataFixtures\UserFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;



class NewsFixtures extends Fixture implements DependentFixtureInterface 
{
public function load(ObjectManager $manager)
{
    for ($i=0; $i<10; $i++)
    {
        $news=new News();
        $news->setTitre('News '.$i);
        $news->setTexte("Lorem ipsum dolor, sit amet consectetur adipisicing elit.");
        $news->setImage('image'.$i);
        $news->setCreatedAt(new \DateTime('now'));
        $news->setUpdatedAt(new \DateTime('now'));
        $news->setActive(1);
        $news->setUser($this->getReference(UserFixtures::USER_ADMIN_REFERENCE));
        $manager->persist($news);

    }
    $manager->flush();
}
public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }

}