<?php 

namespace App\DataFixtures;

use App\Entity\PostChat;
use App\Entity\CategoriePost;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public const POST_REFERENCE = 'post';
    public function load(ObjectManager $manager){
        $categorie = new CategoriePost();
        $categorie->setNom('Devoir');
        $manager->persist($categorie);

            $postChat = new PostChat();
            $postChat->setTexte("J'ai besoin d'aide pour mon devoir de maths");
            $postChat->setCategorie($categorie);
            $postChat->setCreatedAt(new \DateTime('now'));
            $postChat->setUser($this->getReference(UserFixtures::USER_REFERENCE));
            $manager->persist($postChat);
            $this->addReference(self::POST_REFERENCE, $postChat);

            $manager->flush();
    }
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
