<?php 

namespace App\DataFixtures;

use App\Entity\ChatPost;
use App\Entity\Conversation;
use App\DataFixtures\UserFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ConversationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager){

            $conversations = new Conversation();
            $conversations->setReponse("Je peux t'aider volontier !");
            $conversations->setCreatedAt(new \DateTime('now'));
            $conversations->setUser($this->getReference(UserFixtures::USER_REFERENCE));
            $conversations->setPosts($this->getReference(PostFixtures::POST_REFERENCE));
            $manager->persist($conversations);

            $manager->flush();
    }
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            PostFixtures::class
        ];
    }
}