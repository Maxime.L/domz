<?php

namespace App\Entity;

use App\Repository\AvatarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AvatarRepository::class)
 * @Vich\Uploadable
 */
class Avatar implements Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="avatar_upload", fileNameProperty="image")
     * @Assert\Image(
     * minWidth = 200,
     * maxWidth = 600,
     * minHeight = 200,
     * maxHeight = 600,
     * minWidthMessage = "Votre avatar doit faire plus de 200px de largeur.",
     * maxWidthMessage = "Votre avatar ne peut pas faire plus de 600px de largeur.",
     * minHeightMessage = "Votre avatar doit faire plus de 200px de hauteur.",
     * maxHeightMessage = "Votre avatar ne peut pas faire plus de 600px de hauteur.",
     * mimeTypes = { "image/jpeg", "image/jpg", "image/png" },
     * mimeTypesMessage = "Seuls les formats jpg et png sont autorisés."
     * )
     * 
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="avatar")
     */
    private $users;

    // Constructeur
    public function __construct()
    {
        $this->updateAt = new \DateTimeImmutable();
        $this->users = new ArrayCollection();
    }

    // Méthodes
    public function __toString()
    {
        return (!empty($this->image) && !is_null($this->image)) ? $this->image : "Empty avatar"; 
    }

    public function serialize()
    {
        // On convertit le fichier image en base64 (représentation d'une image sous forme de chaîne de caractères)
        $this->imageFile = base64_encode($this->imageFile);
    }
    public function unserialize($serialized)
    {
        // A l'inverse, on convertit la base64 en fichier image
        $this->imageFile = base64_decode($serialized);
    }
    // Getters/Setters
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setAvatar($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getAvatar() === $this) {
                $user->setAvatar(null);
            }
        }

        return $this;
    }
}