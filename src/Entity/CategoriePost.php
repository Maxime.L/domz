<?php

namespace App\Entity;

use App\Repository\CategoriePostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriePostRepository::class)
 */
class CategoriePost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=PostChat::class, mappedBy="categorie", orphanRemoval=true)
     */
    private $postChats;

    public function __construct()
    {
        $this->postChats = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|PostChat[]
     */
    public function getPostChats(): Collection
    {
        return $this->postChats;
    }

    public function addPostChat(PostChat $postChat): self
    {
        if (!$this->postChats->contains($postChat)) {
            $this->postChats[] = $postChat;
            $postChat->setCategorie($this);
        }

        return $this;
    }

    public function removePostChat(PostChat $postChat): self
    {
        if ($this->postChats->removeElement($postChat)) {
            // set the owning side to null (unless already changed)
            if ($postChat->getCategorie() === $this) {
                $postChat->setCategorie(null);
            }
        }

        return $this;
    }
}
