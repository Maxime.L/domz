<?php

namespace App\Form;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserDropDownType extends AbstractType
{
    /**
     * @var UserRepository
     * 
     */
    private $userRepository;

    /**
     * AvailableCategoryType constructor.
     * @param UserRepository $categoryRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'query_builder' => $this->userRepository->adminQuery()
        ]);
    }
    /**
     * @return string|null
     */
    public function getParent()
    {
        return EntityType::class;
    }
}
