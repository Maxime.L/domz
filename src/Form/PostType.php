<?php

namespace App\Form;

use App\Entity\PostChat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('texte')
            ->remove('createdAt')
            ->remove('updatedAt')
            ->add('user')
            ->add('categorie')
        ;
    }
}
